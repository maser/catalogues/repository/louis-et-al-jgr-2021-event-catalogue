# Louis et al JGR 2021 - Event Catalogue

This script has been used to produce the supplementary material TFCat version 
of Louis et al. (2022).

The TFCat file is prepared from an IDL saveset file containg a series of 
typed Jovian radio emission features. 

## Run the example

```bash
python load_raw_data.py
python prepare_tfcat.py
```

## Output
The yearly TFCat files are produced in the `build/` directory.

The command line output are useful for managing the data on the MASER repository: 
- SPIP table content for the landing page
- scp commands to copy the created files on MASER's data server

## References

- Louis, C. K., Zarka, P., Dabidin, K., Lampson, P.-A., Magalhães, F. P., 
  Boudouma, A., et al. (**2021**). *Latitudinal Beaming of Jupiter’s Radio 
  Emissions From Juno/Waves Flux Density Measurements*. J. Geophys. Res. Space 
  Physics, 126, 1–23. [doi:10.1029/2021ja029435](https://doi.org/10.1029/2021ja029435)
