#!/usr/bin/env python3
# -*- encode: utf-8 -*-

import requests
from pathlib import Path

RAW_DATA_URL = "https://maser.obspm.fr/doi/10.25935/nhb2-wy29/content/sav/emissions_catalog_v1.sav"
RAW_DATA_DIR = Path(__file__).parent / "data"
RAW_DATA_DIR.mkdir(parents=True, exist_ok=True)
RAW_DATA_FILE = RAW_DATA_DIR /  RAW_DATA_URL.split('/')[-1]

print(f"Downloading raw data from {RAW_DATA_URL}")
r = requests.get(RAW_DATA_URL, allow_redirects=True)
if r.status_code == 200:
    RAW_DATA_FILE.unlink(missing_ok=True)
    open(RAW_DATA_FILE, 'wb').write(r.content)
    print("Saved")
else:
    print(f"Error downloading raw data from {RAW_DATA_URL}")

