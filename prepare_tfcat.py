#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
MIT License

Copyright (c) 2021 Baptiste Cecconi (Observatoire de Paris, PADC/MASER)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


__author__ = 'Baptiste Cecconi'
__copyright__ = 'Copyright 2024, Baptiste Cecconi, PADC/MASER'
__credits__ = ['Baptiste Cecconi']
__license__ = "MIT"
__version__ = "1.0.2"
__maintainer__ = 'Baptiste Cecconi'
__email__ = 'baptiste.cecconi@obspm.fr'
__status__ = "production"

from tfcat.feature import Feature, FeatureCollection, CRS
from tfcat.geometry import Polygon
from tfcat.codec import dump
from tfcat.validate import validate_file
from scipy.io import readsav
from pathlib import Path
import numpy
import datetime
from astropy.time import Time
from astropy.units import Unit as u
import sys
import os
from shapely.geometry import LinearRing

SAV_FILE = Path("data") / "emissions_catalog_v1.sav"
SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0.json"


def feature_times(hour_list, yyyyddd):
    """
    Converts a list of start times (hour) and a date into a Time array.

    :param hour_list: list of start times (hours)
    :param yyyyddd: date in YYYY-DDD format (year, day-of-year)
    :return:
    """
    cur_date = datetime.datetime.strptime(str(yyyyddd), "%Y%j")
    return Time(cur_date) + hour_list * u('hr')


def feature_frequencies(freq_indices, freq_map, freq_unit=u('kHz')):
    """
    Converts a list of frequency indices into a frequency list.
    :param freq_indices: list of frequency indices
    :param freq_map: frequency map
    :param freq_unit: frequency unit (defaults to kHz)
    :return:
    """
    return numpy.interp(freq_indices, range(len(freq_map)), freq_map) * freq_unit

# Feature types as described in the paper.
FEATURE_TYPES = {
    "?": "(Unsure or Ambiguous)",
    "anom": "Anomaly",
    "EuA": "Europa-DAM A",
    "EuB": "Europa-DAM B",
    "EuC": "Europa-DAM C",
    "EuD": "Europa-DAM D",
    "HFa": "High Frequency Arcs",
    "GaA": "Ganymede-DAM A",
    "GaB": "Ganymede-DAM B",
    "GaC": "Ganymede-DAM C",
    "GaD": "Ganymede-DAM D",
    "IoA": "Io-DAM A",
    "IoB": "Io-DAM B",
    "IoC": "Io-DAM C",
    "IoD": "Io-DAM D",
    "nLF": "narrowband Low Frequency",
    "nKOM": "nKOM",
    "cal": "Perijove HFR-Low Calibration",
    "QP": "Quasi-Periodic Bursts",
    "SRC": "Radio source region"
}

# mapping from raw data (.sav file) to FEATURE_TYPES
RAW_FEATURE_TYPES_MAPPING = {
    "?": ("?",),
    "A": ("anom",),
    "BK?": None,
    "EA": ("EuA",),
    "EA?": ("EuA", "?"),
    "EB": ("EuB",),
    "EB?": ("EuB", "?"),
    "EC": ("EuC",),
    "EC?": ("EuC", "?"),
    "ED": ("EuD",),
    "ED?": ("EuD", "?"),
    "HFA": None,  # ("HFa",),
    "HFA?": None,  # ("HFa", "?"),
    "GA": ("GaA",),
    "GA?": ("GaA", "?"),
    "GB": ("GaB",),
    "GB?": ("GaB", "?"),
    "GC": ("GaC",),
    "GC?": ("GaC", "?"),
    "GD": ("GaD",),
    "GD?": ("GaD", "?"),
    "I?": None,
    "IA": ("IoA",),
    "IA?": ("IoA", "?"),
    "IB": ("IoB",),
    "IB?": ("IoB", "?"),
    "IC": ("IoC",),
    "IC?": ("IoC", "?"),
    "ID": ("IoD",),
    "ID?": ("IoD", "?"),
    "NB": ("nLF",),
    "NB?": ("nLF", "?"),
    "NK": ("nKOM",),
    "NK?": ("nKOM", "?"),
    "PHC": ("cal",),
    "QP": ("QP",),
    "QP?": ("QP", "?"),
    "S": None,
    "SRC": ("SRC",)
}


def converter(input_file, verbose=False):
    """
    Converts the `emissions_catalog.sav` file into a TFCat JSON file

    :param input_file: Input file path of the emissions catalogue (IDL SAVESET .sav format)
    :type input_file: str or Path
    """

    # loading data from intermediate .sav file
    print('loading data...')

    raw_data = readsav(input_file)
    raw_em = raw_data['em']  # Event metadata
    raw_freq = raw_data['frequency']  # Frequency list

    feature_t = raw_em['x']  # Time coordinates
    feature_f = raw_em['y']  # Frequency coordinates
    feature_d = raw_em['yyyyddd']  # Year-doy time stamp
    feature_n = raw_em['name']  # Feature type (fixed nomenclature, see keys of RAW_FEATURE_TYPES_MAPPING)

    events = list()  # Stem for list of events
    ftypes = set()  # Stem for set of feature types (using a set here to avoid duplicated values)

    for i, (t, f, d, n) in enumerate(zip(feature_t, feature_f, feature_d, feature_n)):

        # checking feature type (if feature_tuple is None, skip it)
        feature_tuple = RAW_FEATURE_TYPES_MAPPING[n.decode('ascii').strip()]
        if feature_tuple is None:
            continue

        # adding current feature type into ftypes set
        ftypes.add(feature_tuple)

        # creating mask to keep only valid polygon points
        m = f > 0

        # creating raw time and freq lists
        tt, ff = map(lambda x: list(x[m]) + [x[0]], (t, f))

        # packing coordinates into a list
        coords = list(zip(feature_times(tt, d).unix, feature_frequencies(ff, raw_freq).value))

        # checking if list of coordinates is curled in the direct trigonometric sense (counter-clock-wise)
        if not LinearRing(coords).is_ccw:
            coords = coords[::-1]

        events.append(Feature(
            id=i,
            # converting into physical values (unix time and kHz))
            geometry=Polygon([coords]),
            properties={
                'feature_type': "".join(feature_tuple)
            }
        ))

    crs = CRS({
        "type": "local",
        "properties": {
            "name": "Time-Frequency",
            "time_coords_id": "unix",
            "spectral_coords": {
                "type": "frequency",
                "unit": "kHz"
            },
            "ref_position_id": "juno"
        }
    })

    fields = {
        'feature_type': {
            'info': 'Feature Type',
            'datatype': 'str',
            'ucd': 'meta.id',
            'values': dict(
                [("".join(ft), " ".join(map(FEATURE_TYPES.get, ft))) for ft in ftypes])
        }
    }

    properties = {
        "instrument_host_name": "Juno",
        "instrument_name": "Waves",
        "title": "Catalogue of Juno Radio emissions",
        "authors": [{
            "GivenName": "Corentin K.",
            "FamilyName": "Louis",
            "ORCID": "https://orcid.org/0000-0002-9552-8822",
            "Affiliation": "https://ror.org/051sx6d27"
        },],
        "target_name": "Jupiter",
        "target_class": "planet",
        "target_region": "magnetosphere",
        "feature_name": "radio emissions",
        "bib_reference": "https://doi.org/10.1029/2021JA029435",
        "data_source_reference": "https://doi.org/10.25935/6jg4-mk86",
        "doi": "10.25935/nhb2-wy29",
        "publisher": "PADC/MASER",
        "version": "1.0"
    }

    yearly_catalogues = dict([(str(year), []) for year in range(2016, 2021)])
    for feature in events:
        tmin_year = crs.time_converter(feature.tmin).isot[0:4]
        yearly_catalogues[tmin_year].append(feature)

    maser_doi = properties['doi'].lower()
    maser_content_url = f'https://maser.obspm.fr/doi/{maser_doi}/content/'

    print('\nSPIP Table:\n-----------\n')
    print(f'|{{{{Year}}}}|{{{{# of Events}}}}|{{{{Size (MB)}}}}|{{{{File}}}}|{{{{Start Time}}}}|{{{{End Time}}}}|')
    for year, year_catalogue in yearly_catalogues.items():
        if len(year_catalogue) != 0:
            year_collection = FeatureCollection(
                schema=SCHEMA_URI,
                features=year_catalogue,
                properties=properties,
                fields=fields,
                crs=crs
            )
            year_collection.properties['time_min'] = crs.time_converter(year_catalogue[0].tmin).isot
            year_collection.properties['time_max'] = crs.time_converter(year_catalogue[-1].tmax).isot

            year_file = Path('build') / f'juno_waves_lesia_tfcat_{year}.json'
            with open(year_file, 'w') as f:
                dump(year_collection, f)
            validate_file(year_file, SCHEMA_URI)

            file_size = os.path.getsize(year_file) / (1024 * 1024)
            print(f'|{year}|{len(year_catalogue)}|{file_size:.2f}|' +
                  f'[{year_file.name}->{maser_content_url}{year_file.name}]|' +
                  f'{year_collection.properties["time_min"]}|{year_collection.properties["time_max"]}|')
        else:
            yearly_catalogues[year] = None

    print('\nscp commands:\n-------------\n')
    for year in yearly_catalogues:
        if yearly_catalogues[year] is not None:
            print(f"scp build/juno_waves_lesia_tfcat_{year}.json vmmaser_rpws:/volumes/kronos/doi/{maser_doi}/content/data/")
    print()


def validate(file_name):
    from tfcat.validate import validate_file
    validate_file(file_name, schema_uri="https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0.json")


if __name__ == "__main__":

    args = sys.argv[1:]

    verbose = False
    if '-v' in args:
        verbose = True
        args.remove('-v')

    if len(args) == 0:
        input_file = Path(SAV_FILE)
    else:
        input_file = Path(args[0])

    if not input_file.exists():
        raise FileNotFoundError(f"Input file doesn't exist: '{input_file}'")

    if len(args) > 1:
        raise IndexError('Too many arguments')

    if verbose:
        print(f'Input file: {input_file}')

    converter(str(input_file), verbose=verbose)

